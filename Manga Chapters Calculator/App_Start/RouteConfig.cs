﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Manga_Chapters_Calculator
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                constraints: new { controller = "(?!Mangas).*" }
            );

            routes.MapRoute(
                name: "MangaPost",
                url: "Mangas/{action}",
                defaults: new { controller = "Mangas", action = "" },
                constraints: new { action = "(?=GetGlobalChapter).*" }
            );

            routes.MapRoute(
                name: "Manga",
                url: "MangaFamilies/{MangaFamilyID}/{controller}/{action}/{partNo}",
                defaults: new { action = "Index", partNo = UrlParameter.Optional, MangaFamilyID = UrlParameter.Optional },
                constraints: new { controller = "Mangas" }
            );

        }
    }
}
