﻿using Manga_Chapters_Calculator.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Manga_Chapters_Calculator.DAL
{
    public class MangaDBInitializer : DropCreateDatabaseAlways<MangaContext>
    {
        protected override void Seed(MangaContext context)
        {
            IList<MangaFamily> families = new List<MangaFamily>();
            IList<Manga> mangas = new List<Manga>();

            families.Add(new MangaFamily() { ID = 1, Name = "Jojo no Kimyou na Bouken" });
            families.Add(new MangaFamily() { ID = 2, Name = "Higurashi no Naku Koro ni" });
            families.Add(new MangaFamily() { ID = 3, Name = "Durarara!!" });

            //Jojo
            mangas.Add(new Manga() { MangaFamilyID = 1, PartNo = 1, Name = "Phantom Blood", ChapterCount = 44 });
            mangas.Add(new Manga() { MangaFamilyID = 1, PartNo = 2, Name = "Sentou Chuuryuu", ChapterCount = 69 });
            mangas.Add(new Manga() { MangaFamilyID = 1, PartNo = 3, Name = "Stardust Crusaders", ChapterCount = 152 });
            mangas.Add(new Manga() { MangaFamilyID = 1, PartNo = 4, Name = "Diamond wa Kudakenai", ChapterCount = 174 });
            mangas.Add(new Manga() { MangaFamilyID = 1, PartNo = 5, Name = "Ougon no Kaze", ChapterCount = 155 });
            mangas.Add(new Manga() { MangaFamilyID = 1, PartNo = 6, Name = "Stone Ocean", ChapterCount = 158 });
            mangas.Add(new Manga() { MangaFamilyID = 1, PartNo = 7, Name = "Steel Ball Run", ChapterCount = 96 });
            mangas.Add(new Manga() { MangaFamilyID = 1, PartNo = 8, Name = "JoJolion", ChapterCount = 0 });

            //Higurashi
            mangas.Add(new Manga() { MangaFamilyID = 2, PartNo = 1, Name = "Onikakushi-hen", ChapterCount = 7 });
            mangas.Add(new Manga() { MangaFamilyID = 2, PartNo = 2, Name = "Watanagashi-hen", ChapterCount = 12 });
            mangas.Add(new Manga() { MangaFamilyID = 2, PartNo = 3, Name = "Tatarigoroshi-hen", ChapterCount = 13 });
            mangas.Add(new Manga() { MangaFamilyID = 2, PartNo = 4, Name = "Himatsubushi-hen", ChapterCount = 9 });
            mangas.Add(new Manga() { MangaFamilyID = 2, PartNo = 5, Name = "Kai: Meakashi-hen", ChapterCount = 21 });
            mangas.Add(new Manga() { MangaFamilyID = 2, PartNo = 6, Name = "Kai: Tsumihoroboshi-hen", ChapterCount = 16 });
            mangas.Add(new Manga() { MangaFamilyID = 2, PartNo = 7, Name = "Kai: Minagoroshi-hen", ChapterCount = 25 });
            mangas.Add(new Manga() { MangaFamilyID = 2, PartNo = 8, Name = "Kai: Matsuribayashi-hen", ChapterCount = 35 });

            //Durarara
            mangas.Add(new Manga() { MangaFamilyID = 3, PartNo = 1, Name = "", ChapterCount = 22 });
            mangas.Add(new Manga() { MangaFamilyID = 3, PartNo = 2, Name = "Saika-hen", ChapterCount = 16 });
            mangas.Add(new Manga() { MangaFamilyID = 3, PartNo = 3, Name = "Kokinzoku-hen", ChapterCount = 19 });
            mangas.Add(new Manga() { MangaFamilyID = 3, PartNo = 4, Name = "RE;Dollars-hen", ChapterCount = 0 });


            foreach(MangaFamily family in families)
            {
                context.MangaFamilies.Add(family);
            }

            foreach (Manga manga in mangas)
            {
                context.Mangas.Add(manga);
            }

            base.Seed(context);
        }
    }
}