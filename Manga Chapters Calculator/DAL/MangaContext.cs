﻿using Manga_Chapters_Calculator.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Manga_Chapters_Calculator.DAL
{
    public class MangaContext : DbContext
    {
        public MangaContext() : base("MangaContext")
        {
            Database.SetInitializer(new MangaDBInitializer());
        }

        public DbSet<MangaFamily> MangaFamilies { get; set; }
        public DbSet<Manga> Mangas { get; set; }



        public List<Manga> mangasFromFamily(string familyId, bool includeFamily = true)
        {
            List<Manga> mangas;
            if(includeFamily)
                mangas = this.Mangas.Where(manga => manga.MangaFamilyID.ToString() == familyId)
                    .OrderBy(manga => manga.PartNo)
                    .Include(m => m.MangaFamily)
                    .ToList();
            else
                mangas = this.Mangas.Where(manga => manga.MangaFamilyID.ToString() == familyId).ToList();

            return mangas;
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}