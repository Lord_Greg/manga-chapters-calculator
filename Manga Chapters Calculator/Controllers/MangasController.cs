﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Manga_Chapters_Calculator.Models;
using Manga_Chapters_Calculator.DAL;

namespace Manga_Chapters_Calculator.Controllers
{
    public class MangasController : Controller
    {
        private MangaContext db = new MangaContext();

        // GET: Mangas
        public ActionResult Index()
        {
            var mangas = db.mangasFromFamily(getFamilyId());
            return View(mangas);
        }

        // GET: Mangas/Details/5
        public ActionResult Details(int? partNo)
        {
            if (partNo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manga manga = findManga(partNo);
            if (manga == null)
            {
                return HttpNotFound();
            }
            return View(manga);
        }

        // GET: Mangas/Create
        public ActionResult Create()
        {
            ViewBag.MangaFamilyID = new SelectList(db.MangaFamilies, "ID", "Name");
            return View();
        }

        // POST: Mangas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,PartNo,ChapterCount,MangaFamilyID")] Manga manga)
        {
            if (ModelState.IsValid)
            {
                db.Mangas.Add(manga);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MangaFamilyID = new SelectList(db.MangaFamilies, "ID", "Name", manga.MangaFamilyID);
            return View(manga);
        }

        // GET: Mangas/Edit/5
        public ActionResult Edit(int? partNo)
        {
            if (partNo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manga manga = findManga(partNo);
            if (manga == null)
            {
                return HttpNotFound();
            }
            ViewBag.MangaFamilyID = new SelectList(db.MangaFamilies, "ID", "Name", manga.MangaFamilyID);
            return View(manga);
        }

        // POST: Mangas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,PartNo,ChapterCount,MangaFamilyID")] Manga manga)
        {
            if (ModelState.IsValid)
            {
                db.Entry(manga).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MangaFamilyID = new SelectList(db.MangaFamilies, "ID", "Name", manga.MangaFamilyID);
            return View(manga);
        }

        // GET: Mangas/Delete/5
        public ActionResult Delete(int? partNo)
        {
            if (partNo == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manga manga = findManga(partNo);
            if (manga == null)
            {
                return HttpNotFound();
            }
            return View(manga);
        }

        // POST: Mangas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int partNo)
        {
            Manga manga = findManga(partNo);
            db.Mangas.Remove(manga);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult GetGlobalChapter(int chapterLocal, int mangaId)
        {
            int chapterGlobal = chapterLocal;
            Manga currentManga = db.Mangas.Find(mangaId);

            if (currentManga != null)
            {
                List<Manga> mangas = db.Mangas.Where(
                        x => x.MangaFamilyID == currentManga.MangaFamilyID &&
                        x.PartNo < currentManga.PartNo
                    ).ToList<Manga>();

                foreach(Manga manga in mangas)
                {
                    chapterGlobal += manga.ChapterCount;
                }
            }

            return Json(new { resultChapter = chapterGlobal }, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



        private string getFamilyId()
        {
            return Url.RequestContext.RouteData.Values["MangaFamilyID"].ToString();
        }

        private Manga findManga(int? partNo, string familyId = null)
        {
            if (familyId == null) familyId = getFamilyId();
            var mangas = db.Mangas.Where(
                element => element.MangaFamilyID.ToString() == familyId && element.PartNo == partNo
                );
            if(mangas.Count() > 0)
                return mangas.First();

            return null;
        }
    }
}
