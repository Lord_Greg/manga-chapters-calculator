﻿using Manga_Chapters_Calculator.DAL;
using Manga_Chapters_Calculator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Manga_Chapters_Calculator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            MangaContext db = new MangaContext();
            List<MangaFamily> families = new List<MangaFamily>(db.MangaFamilies);
            families.Insert(0, new MangaFamily() { ID = -1, Name = " " });

            List<Manga> defaultMangas = new List<Manga>();
            defaultMangas.Add(new Manga() { ID = -1, Name = " " } );

            ViewData["families"] = new SelectList(families, "ID", "Name");
            ViewData["mangas"] = new SelectList(defaultMangas, "ID", "Name");

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult Mangas(int familyId)
        {
            MangaContext db = new MangaContext();
            List<Manga> mangas = new List<Manga>(db.Mangas.Where(x => x.MangaFamilyID == familyId));
            mangas.Sort((x, y) => x.PartNo.CompareTo(y.PartNo));
            mangas.Insert(0, new Manga() { ID = -1, Name = " " });

            return Json(
                mangas.Select(x => new
                {
                    Id = x.ID,
                    Name = x.Name
                })
                , JsonRequestBehavior.AllowGet
            );
        }
    }
}