﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Manga_Chapters_Calculator.Models;
using Manga_Chapters_Calculator.DAL;

namespace Manga_Chapters_Calculator.Controllers
{
    public class MangaFamiliesController : Controller
    {
        private MangaContext db = new MangaContext();

        // GET: MangaFamilies
        public ActionResult Index()
        {
            return View(db.MangaFamilies.ToList());
        }

        // GET: MangaFamilies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MangaFamily mangaFamily = db.MangaFamilies.Find(id);
            if (mangaFamily == null)
            {
                return HttpNotFound();
            }
            return View(mangaFamily);
        }

        // GET: MangaFamilies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MangaFamilies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name")] MangaFamily mangaFamily)
        {
            if (ModelState.IsValid)
            {
                db.MangaFamilies.Add(mangaFamily);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mangaFamily);
        }

        // GET: MangaFamilies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MangaFamily mangaFamily = db.MangaFamilies.Find(id);
            if (mangaFamily == null)
            {
                return HttpNotFound();
            }
            return View(mangaFamily);
        }

        // POST: MangaFamilies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name")] MangaFamily mangaFamily)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mangaFamily).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mangaFamily);
        }

        // GET: MangaFamilies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MangaFamily mangaFamily = db.MangaFamilies.Find(id);
            if (mangaFamily == null)
            {
                return HttpNotFound();
            }
            return View(mangaFamily);
        }

        // POST: MangaFamilies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MangaFamily mangaFamily = db.MangaFamilies.Find(id);
            db.MangaFamilies.Remove(mangaFamily);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult GetLocalChapter(int chapterGlobal, int familyId)
        {
            int chapterLocal = chapterGlobal, mangaId = 0;
            List<Manga> mangas = db.mangasFromFamily(familyId.ToString());
            mangas.OrderBy(x => x.PartNo);

            if (mangas.Count > 0)
            {
                foreach (Manga manga in mangas)
                {
                    if (chapterLocal > manga.ChapterCount)
                    {
                        chapterLocal -= manga.ChapterCount;
                    }
                    else
                    {
                        mangaId = manga.ID;
                        break;
                    }
                }

                if (mangaId == 0)
                {
                    mangaId = mangas.Last().ID;
                }
            }

            return Json(new { resultChapter = chapterLocal, mangaId = mangaId }, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
