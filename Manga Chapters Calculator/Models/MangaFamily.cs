﻿using System.Collections.Generic;

namespace Manga_Chapters_Calculator.Models
{
    public class MangaFamily
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Manga> Mangas { get; set; }
    }
}
