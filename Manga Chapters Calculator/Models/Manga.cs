﻿namespace Manga_Chapters_Calculator.Models
{
    public class Manga
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int PartNo { get; set; }
        public int ChapterCount { get; set; }
        public int MangaFamilyID { get; set; }

        public virtual MangaFamily MangaFamily { get; set; }
    }
}
