﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Manga_Chapters_Calculator.Startup))]
namespace Manga_Chapters_Calculator
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
