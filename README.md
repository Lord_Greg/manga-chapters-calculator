# Manga Chapters Calculator #
Aplikacja służąca do ustalania globalnego numeru rozdziału mangi, w sytuacji gdy składa się ona z wielu osobnych części (jak *JoJo no Kimyou na Bouken*, czy *Higurashi no Naku Koro ni*). Działa także w drugą stronę.

Moja pierwsza aplikacja w ASP.NET.